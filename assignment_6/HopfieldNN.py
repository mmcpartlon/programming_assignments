from utils.datastructures.graph.Graph import Graph
#Applications : graph partitioning into two groups
#
# Do graph partitioning this week


def hopfield_nn(G : Graph):
    """
    given an edge weighted graph G = (V, E)
    we seek weights w_1, ..., w_n for the vertices of G
    such that \sum_{i,j} w_i * w_j * w_e_ij
    is minimized

    :param G:
    :return:
    """

