from abc import ABC, abstractmethod

class LocalSearch(ABC):

    @abstractmethod
    def neighbors(self, curr_state):
        """
        returns a list of states neighboring current state S
        :param instance:
        :param curr:
        :return:
        """
    @abstractmethod
    def get_val(self, state):
        """
        gets the value of the given state
        :param state:
        :return:
        """

    @abstractmethod
    def random_neighbor(self, current_state):
        """
        return a random element from the neighbors
        of the current state
        :param current_state:
        :return:
        """

import numpy as np
def GradientDescent(alg : LocalSearch, starting_state, minimize = True):
    best_state = starting_state
    finished = False
    while not finished:
        # transition to a neighbor of the current
        # state having a better value
        neighbors = alg.neighbors(best_state)
        neighbors.append(best_state)
        vals = np.array([alg.get_val(state) for state in neighbors])
        if not minimize:
            vals = -vals
        best_nbr = neighbors[np.argmin(vals)]
        finished = alg.get_val(best_state) == alg.get_val(best_nbr)
        best_state = best_nbr
    return best_state

def MCMC(alg : LocalSearch, starting_state, transition_func, n_steps=500, minimize = True):
    best_state, best_val = starting_state, alg.get_val(starting_state)
    if not minimize:
        best_val*=-1
    curr_state = starting_state
    for i in range(n_steps):
        # transition to a neighbor of the current
        # state having a better value
        nbr = alg.random_neighbor(curr_state)
        nbr_val = alg.get_val(nbr)
        if not minimize:
            nbr_val*=-1
        if nbr_val<best_val:
            best_val=nbr_val
            best_state=nbr
        if transition_func(best_state,nbr):
            curr_state = nbr
    return best_state

#hopfield network and generalization
