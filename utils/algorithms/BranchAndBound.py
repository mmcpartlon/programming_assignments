#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from abc import ABC, abstractmethod
class Instance(ABC) :

    def __init__(self, args, kwargs) :
        self.args = args
        self.kwargs = kwargs
        self.result = None

    def get_args(self) :
        return self.args

    def get_kwargs(self) :
        return self.kwargs

    def set_args(self, *args):
        self.args = args

    def set_kwargs(self, **kwargs):
        self.kwargs = kwargs

    def has_result(self) :
        return self.set_result

    def set_result(self, result):
        self.result = result

    def get_result(self):
        return self.result

    def clone(self):
        pass

class Solution():

    def __init__(self, sol, val):
        self.sol = sol
        self.val = val

    def get_sol(self):
        return self.sol

    def get_val(self):
        return self.val

    def set_val(self, val):
        self.val = val

    def add_to_sol(self, elt):
        pass

    def remove_from_sol(self, elt):
        pass

    def clone(self):
        pass


class BranchAndBound(ABC):

    def __init__(self, minimization = True):
        self.scale = 1
        if minimization:
            self.scale = -1
    
    @abstractmethod
    def can_prune(self, instance, partial_sol, best_sol):
        """
        Is the partial solution a complete solution?
        Is the partial solution a valid solution?
        :param instance:
        :param partial_sol:
        :param best_sol:
        :return:
        """
        pass

    @abstractmethod
    def bound(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_value(self, solution):
        pass


    @abstractmethod
    def betterQ(self, sol1 : Solution, sol2 : Solution) -> bool:
        """
        :param sol1:
        :param sol2:
        :return: true if sol1 is better than sol2
        """
        v1, v2 = sol1.get_val(), sol2.get_val()
        return self.scale*v1>self.scale*v2
    
    @abstractmethod
    def get_next_choices(self, instance, partial_solution, curr_best):
        pass

    def add_choice(self, instance, partial_sol):
        pass

    def remove_choice(self, instance, partial_sol):

    def get_better(self, sol1, sol2):
        if self.betterQ(sol1, sol2):
            return sol2
        return sol1

    def branch_and_bound(self, instance, partial_sol : Solution, best_sol : Solution):
        if self.can_prune(instance, partial_sol):
            return self.get_better(partial_sol,best_sol)

        #bound max value in the subtree
        bound = self.scale * self.bound(partial_sol)
        if bound <= self.scale*best_sol:
            return best_sol

        #if we made it here, then there is a possibility that a
        #better solution exists in this subtree

        next_choices = self.get_next_choices(instance, partial_sol, best_sol)
        #make a copy of the original subproblem and instance to be returned for
        #backtraction
        partial_sol_copy = partial_sol.clone()
        instance_copy = instance.clone()

        for choice in next_choices:
            #i. generate a partial solution P' and subproblem
            # S' with that choice made
            partial_sol.add_to_sol(choice)
            instance.update(choice, )

            #update the value of the solution
            new_val = self.get_value(partial_sol)
            partial_sol.set_val(new_val)



            #restore partial solution and instance to their original state
            partial_sol.remove_from_sol(choice)
            instance.remove_choice(choice)





