import sys
pth = '/home/mmcpartlon/programming_assignments'
if pth not in sys.path :
    sys.path.append(pth)

import numpy as np
from assignment_3.SkipList import SkipList
height, p = 10, 0.5
skip_list = SkipList(height=5, p=0.5)


n_items = 100
items_to_add = np.random.randint(0, 2**height, n_items)
#add all the items

for item in items_to_add:
    skip_list.add(item)
    assert item in skip_list

print('skip list size :',skip_list.num_vals)
print('skip list num elts:',skip_list.num_elts)
ratio = skip_list.num_elts/skip_list.num_vals
print('ratio : ',ratio)
print('actual num vals : ',len(set(items_to_add)))

skip_list.remove(items_to_add[0])
print('removed item :',items_to_add[0])
assert items_to_add[0] not in skip_list
print('skip list size :',skip_list.num_vals)
print('skip list num elts:',skip_list.num_elts)

