#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
def DivideAndConquerMin(A):
    n = len(A)
    if n == 0:
        return np.inf
    if n == 1:
        print('got solution',A[0],'for A :',A)
        return A[0]
    #split array into two parts
    mid = n//2
    L, R = A[0:mid], A[mid:]
    smallest_left =  DivideAndConquerMin(L)
    smallest_right = DivideAndConquerMin(R)
    solution = min(smallest_left, smallest_right)
    print('got solution',solution,'for A :',A)
    return solution

A = np.random.randint(0,50,12)
print('A:',A)
print(DivideAndConquerMin(A))

