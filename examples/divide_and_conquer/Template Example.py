#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#Example for finding minimum element in an array
import numpy as np
class MinInstance(Instance):
    
    def __init__(self, arr):
        super().__init__(arr,{})
    
    def size(self):
        return len(self.args)
    
    def baseQ(self):
        return self.size()<=1

class FindMin(DivideNConquer):
    
    def __init__(self):
        super().__init__()
    
    def get_instance(self, A, kwargs={}):
        return MinInstance(A)
        
    def base(self, instance):
        if instance.size()==0:
            return np.inf
        return instance.args[0]
    
    def divide(self, instance):
        n = instance.size()
        mid = instance.size//2
        left = MinInstance(instance.args[0:mid])
        right = MinInstance(instance.args[mid:])
        return (left,right)
    
    def conquer(self, subproblems):
        assert len(subproblems)==2
        l,r = subproblems[0],subproblems[1]
        min_left, min_right = l.get_result(), r.get_result()
        return min(min_left, min_right)
    
#test it
n = 100
A = np.random.randint(0,1e5,n)
proc = FindMin()
print(np.min(A))
print(proc.alg(A))
        
    

