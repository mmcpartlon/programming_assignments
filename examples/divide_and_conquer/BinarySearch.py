#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
def BinarySearch(A,t):
    n = len(A)
    #BASECASE
    if n==0:
        return False
    if n==1:
        return A[0]==t
    mid = n//2
    #DIVIDE STEP
    B = A[mid:]
    if A[mid]>t:
        B=A[:mid]
    #CONQUER STEP
    solution = BinarySearch(B,t)
    return solution


# In[ ]:




