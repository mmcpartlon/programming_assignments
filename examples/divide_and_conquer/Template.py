#!/usr/bin/env python
# coding: utf-8

# In[13]:


from abc import ABC, abstractmethod

class Instance(ABC):
    
    def __init__(self, args, kwargs):
        self.args = args
        self.kwargs = kwargs
        self.result = None
        
    def args(self):
        return self.args
    
    def kwargs(self):
        return self.kwargs
        
    def has_result(self):
        return self.set_result
    
    def set_result(self, result):
        self.result = result
        
    def get_result(self):
        return self.result
    
    @abstractmethod
    def size(self):
        pass
    
    @abstractmethod
    def baseQ(self):
        pass


class DivideNConquer(ABC):
    
    def __init__(self):
        pass
    
    @abstractmethod
    def base(self, instance):
        """
        return result on a base case instance
        """
        pass
    
    @abstractmethod
    def divide(self, instance):
        """
        returns division of the input (subproblem instances to solve)
        the division should be list of instances.
        """
        pass
    
    @abstractmethod
    def conquer(self, instances):
        """
        perform the conquer step given a list of instances with results
        """
        pass
    
    @abstractmethod
    def get_instance(self, args, kwargs={}):
        pass
    
    def smallerQ(self, problem, subproblem):
        return subproblem.size()<problem.size()
    
    def alg(self, *args, **kwargs):
        instance = self.get_instance(args, kwargs)
        return self._alg(instance)
    
    def _alg(self, instance):
        if instance.baseQ():
            instance.set_result(self.base(instance))
            return
        else:
            subproblems = self.divide(instance)
            for s in subproblems:
                #check that the subproblems are getting smaller
                assert self.smallerQ(instance,s)
                result = self._alg(s)
                s.set_result(result)
            final_result = self.conquer(subproblems)
            return final_result
        
            


# In[ ]:





# In[ ]:




