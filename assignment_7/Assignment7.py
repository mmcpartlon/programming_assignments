#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from abc import abstractmethod ABC
def UnivariateFunc(ABC):
    
    def __init__(self, lb = -1e10, ub = 1e10):
        self.lb = lb
        self.ub = ub
    
    @abstractmethod
    def _eval_func(self, x):
        pass
        
    def eval_func(self, x)
        assert self.lb<x<self.ub
        return self._eval_func(x)
    
    def eval_deriv(self, x, approx = True):
        if approx : 
            self.approx_deriv(x)
        else:
            self._eval_deriv(x)
    
    @abstractmethod
    def _eval_deriv(self, x):
        pass

    def approx_deriv(self, x, eps = 1e-7):
        f0 = self.eval_func(x)
        f1 = self.eval_func(x+eps)
        return (f1-f0)/eps
    
    def get_lb(self):
        return self.lb
    
    def get_ub(self):
        return self.ub


# In[ ]:


from abc import abstractmethod ABC
class RootFinder(ABC):
    
    def __init__(self, warn = True):
        self.warn = warn
    
    def setup_for_root_find(self, func, x, max_evals=1e4, tol = 1e-6):
        assert isinstance(func,UnivariateFunc)
        self.func = func
        self.tol = tol
        self.max_evals = max_evals
        self.f_evals = 0
        self.best_x = x
        self.best_f = self.func.eval_func(x)
    
    @abstractmethod
    def find_root(self, func, *args, **kwargs):
        pass
    
    def eval_func(self, x):
        #will pad one extra alotted evaluation of f so that
        #calls to eval_func and eval_deriv in succession do not 
        #throw assertion errors
        assert not self.finishedQ() or self.f_evals < self.max_evals + 2
        self.f_evals+=1
        f_x = self.func.eval_func(x)
        if self.best_f > abs(f_x):
            self.best_x = x
            self.best_f = abs(f_x)
        return f_x
    
    def eval_deriv(self, x, approx = True):
        #will pad one extra alotted evaluation of f so that
        #calls to eval_func and eval_deriv in succession do not 
        #throw assertion errors
        assert not self.finishedQ() or self.f_evals < self.max_evals + 2
        self.f_evals+=1
        return self.func.eval_deriv(x, approx = approx)
    
    def finishedQ(self):
        t1 = self.f_evals>self.max_evals
        t2 = self.best_f < self.tol
        return t1 or t2
    
    def get_sol(self):
        assert self.finishedQ()
        if self.best_f<self.tol:
            return self.best_x
        if self.f_evals>=self.max_evals and self.warn:
            s = 'WARNING, maximum number of f_evals exceeded, '
            s+= 'but we have failed to converge on a root'
            print(s)
            print('Best solution : ',self.func.eval_func(self.best_x))
            return self.best_x
        return None
                              
                              


# In[ ]:


import numpy as np
class MCMC(RootFinder):
    
    def __init__(self):
        super().__init__()
    
    @abstractmethod
    def find_root(self, func, x0, max_evals = 1e4, tol = 1e-5, *args, **kwargs):
        """
        attempts to find the root of the function func beginning from
        point x0 
       
        @return a point x satisfying | func(x) | <= tol
        if such a point exists, else returns best solution seen during search
        (obtained by calling super().get_sol())
        """
        x0 = self.sample_starting_point(func.get_lb(), func.get_ub())
        self.setup_for_root_find(func, x0, max_evals = max_evals, tol = tol)
        while not self.finishedQ():
            x0 = self.MCMC_step(x0, *args, **kwargs)
        
    def MCMC_step(self, x0):
        #implement one MCMC step to change the current guess x0
        #you may want to add additional parameters such as,
        # an acceptance probability for |f| increasing moves
        # you may even make this acceptance probability depend on the 
        # number of f evaluations, or the current iteration (which would
        #make this simulated annealing)
        #you may wish to try larger changes for x0 when |f(x0)| is far from 0,
        #and smaller changes when it is close... the step size may be random
        pass
    
    def sample_starting_point(self, lb, ub, n_points=100):
        #OPTIONAL - before beginning root-finding,
        #you may wish to sample a starting point x0 from a 
        #distribution over f(x) with x in range lb to ub
        # to sample over a distribution favoring mass on the 
        #roots of a function, we create a distribution D where
        # D(x) ~ exp(-|f(x)|/r)/Z, where Z is a normalizing constant,
        # and r is a scaling constant to prevent overflow.
        # note that points x where f(x) ~ 0 have the largest mass
        
        #REMOVE IF YOU WISH TO IMPLEMENT SAMPLING for starting point
        return (ub+lb)/2
    


# In[ ]:


import numpy as np
class BracketNBisect(RootFinder):
    
    def __init__(self):
        super().__init__()
    
    @abstractmethod
    def find_root(self, func, lb, ub, max_evals = 1e4, tol = 1e-5 ):
        """
        attempts to find the root of the function func
        in the interval [lb,ub]
        @param lb lower bound on x for func(x) evaluations
        @param ub upper bound on x for func(x) evaluations
        @return a point x such that lb<=x<=ub satisfying | func(x) | <= tol
        if such a point exists, else returns best solution seen during search
        (obtained by calling super().get_sol())
        """
        self.setup_for_root_find(func, lb+ub/2, max_evals = max_evals, tol = tol)
        bracket = None
        while bracket is None and not self.finishedQ():
            #attempt to bracket a root of func
            pass
        
        #if bracket is still None at this point, then 
        #we could not bracket a root in the alotted num f evaluations
        #we will return the best value seen during bracket search
        
        if bracket is None:
            return self.get_sol()
        
        start, end = bracket
        while not self.finishedQ():
            #attempt to find a root of f via the bisection method in the interval
            # start, end via bisection
            self.bisect()
        
        return self.get_sol()
    
    def bracket_root(self, lb, ub, n_bracks):
        """
        attempts to find an interval [start,end] such that
        sign(func(start)) == -sign(f(end))
        @param lb lower bound on x for func(x) evaluations
        @param ub upper bound on x for func(x) evaluations
        @return a pair of points (start, end) such that
        sign(f(start))!=sign(f(end)), or returns None if no such
        points can be found in the alotted time.
        """
        #NOTE : all function evaluations must be done via super().eval_func(x)
        intervals = self.get_intervals(lb, ub, n_bracks)
        curr_lb, curr_ub = intervals[0]
        sign_change = self.eval_func(curr_lb)*self.eval_func(curr_ub)<0
        while not self.finishedQ() and not sign_change:
            #TODO
            pass
        
    
    def get_intervals(self, lb, ub, n):
        """
        return n evenly spaced intervals between lb and ub
        """
        intervals = np.empty((n,2))
        cutoffs = np.linspace(lb, ub, n+1)
        intervals[:,0] = cutoffs[:-1]
        intervals[:,1] = cutoffs[1:]
        return intervals
    
    def bisect(self, lb, ub):
        """
        attempt to find a root in the range [lb,ub], via the bisection method
        #NOTE : does not need a return, as the solution is stored by the 
        super class (assuming eval_func is called via super().eval_func )
        """
        #NOTE : all function evaluations must be done via super().eval_func(x)
        
        while not self.finishedQ():
            #attempt to find a root via the bisection method
            pass

        


# In[ ]:


import numpy as np
class NewtonRaphson(RootFinder):
    """
    Find the root of a function via the newton - raphson method of truncating the
    taylor series
    """
    
    def __init__(self):
        super.__init__()
        
    def find_root(self, func, x0, max_evals = 1e4, tol = 1e-5 ):
        """
        attempts to find the root of the function func via the Newton-Raphson method
        @return a point x such that | func(x) | <= tol
        if such a point exists, else returns best solution seen during search 
        (obtained by calling super().get_sol())
        """
        self.setup_for_root_find(func, lb+ub/2, max_evals = max_evals, tol = tol)
        while not self.finishedQ():
            x0 = self.newton_raphson_step(x0)
        return super.get_sol()
    
    def newton_raphson_step(self, x0):
        pass
        #TODO - implement one step of Newton - Raphson method


# In[ ]:


class Secant(RootFinder):
    """
    TODO
    """
    
    def __init__(self):
        pass
    


# In[ ]:


"""
Represents the univariate function f(x + delta*d) w.r.t the variable delta,
when x and d are given

To be used in gradient descent with multivariate function as input
"""

class DirectionalFunc(UnivariateFunc):
    
    def __init__(self, func, lb = -1e10, ub = 1e10):
        self.func = func
        
    def set_direction(self, d):
        self.direction = d
    
    def set_x(self, x)?:
        self.x = x
        
    def _eval_func(self, delta):
        return self.func.eval_func(self.x+self.d*delta)


# In[ ]:


"""
TODO - gradient descent and other minimization techniques
"""

