from utils.algorithms.BranchAndBound import BranchAndBound, Instance, Solution


class WCInstance(Instance):
    """
    representation of a weighted clique instance
    as a subgraph G[S], where S is a subset of vertices,
    C is the current clique, and
    c~s for all s \in S, c \in C
    """
    def __init__(self, C, S, G):
        super.__init__(self)
        self.S = self.filter(C, G, S)
        self.G = G

    def



class WeightedClique(BranchAndBound):

    def __init__(self, G):
        super().__init__(minimization=False)
        self.G = G

    def can_prune(self, instance, partial_sol, best_sol):
        #get the sum of weight of vertices we can (possibly)
        #add
        remaining_verts = instance.get_args()
        rem_wt = [v.get]

    def get_instance(self, G, choice):
