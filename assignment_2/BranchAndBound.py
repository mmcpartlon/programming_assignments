from abc import ABC, abstractmethod

def BranchAndBound(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def branch(self, subproblem):
        """
        :param self:
        :param subproblem:
        :return:
        """

    @abstractmethod
    def bound(self, subproblem):
        """

        :param self:
        :param subproblem:
        :return:
        """

    @abstractmethod
    def value(self, subproblem):
        """

        :param self:
        :param subproblem:
        :return:
        """

    def BranchAndBound(self, instance):
        #TODO
