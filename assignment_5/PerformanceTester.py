from assignment_5.BloomFilterABC import BloomFilterABC

def test_performance(bf : BloomFilterABC, data_to_add, data_to_check):
    true_data = set([d for d in data_to_add])
    #add data points to bloom filter
    for d in data_to_add:
        bf.add(d)
    true_positives = [d for d in data_to_check if d in true_data]
    bf_positives = [d for d in data_to_check if d in bf]
    num_bf_positives = len(bf_positives)
    diff = set(bf_positives)-set(true_positives)
    true_negatives = [d for d in data_to_check if d not in true_data]
    num_bf_negatives = len(data_to_check)-len(bf_positives)
    # 1 - (|diff|/|num true positives|)
    s1 = (1-(len(diff)/len(true_positives)))*50
    s2 = (1 - (num_bf_negatives/len(true_negatives)))*50
    return s1+s2
