from assignment_5.BloomFilterABC import BloomFilterABC
class BasicFilter(BloomFilterABC):

    def __init__(self, capacity : int, ty):
        super().__init__(capacity,ty)

    def _add(self, item):
        #TODO
        pass

    def _remove(self, item):
        #TODO
        pass

    def _hash(self, item):
        #TODO
        pass

    def __contains__(self, item):
        #TODO
        pass

class MultiHashFilter(BloomFilterABC):
    """

    """

    def __init__(self, capacity: int, ty, num_hfs: int = 3) :
        """

        :param capacity:
        :param ty:
        :param num_hfs: number of hash functions and corresponding
        tables to use
        """
        super().__init__(capacity, ty)

    def _add(self, item) :
        # TODO
        pass

    def _remove(self, item) :
        # TODO
        pass

    def _hash(self, item) :
        # TODO
        pass

    def __contains__(self, item) :
        # TODO
        pass