from abc import ABC, abstractmethod

class BloomFilterABC(ABC):

    def __init__(self, capacity : int, ty):
        self.num_added = 0
        self.num_filled = 0
        self.capacity = capacity
        self.ty = ty


    @abstractmethod
    def __contains__(self, item):
        pass

    @abstractmethod
    def _add(self, item):
        """
        adds the item to the bloom filter and returns true if
        hash of the item is not already in the filter
        :param item:  item to add
        :return: true iff hash of item is not in this filter
        """
        pass

    def add(self, item):
        temp = item in self
        added = self._add(item)
        assert item in self
        if added:
            assert not temp
            self.num_added+=1
            self.num_filled+=1

    def add_all(self, *items):
        for item in items:
            self.add(item)

    @abstractmethod
    def _remove(self, item):
        """
        removes the item from the bloom filter and returns true
        the hash of the item is not already in the bloom filter
        :param item: the item to remove
        :return: true iff item was in bloom filter
        """
        pass

    def remove(self, item):
        temp = item not in self
        removed = self._remove(item)
        assert item not in self
        if removed:
            assert not temp
            self.num_filled-=1

    def remove_all(self, *items):
        for item in items:
            self.remove(item)

    @abstractmethod
    def _hash(self, item):
        pass

    def hash(self, item):
        assert isinstance(item,self.ty)
        return self.hash(item)


