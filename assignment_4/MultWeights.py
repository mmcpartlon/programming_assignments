import numpy as np

class MultiplicativeWeights():

    def __init__(self, discount = 0.5):
        self.beta = discount
        self.experts = []
        self.weights = []

    def add_expert(self, expert):
        self.experts.append(expert)
        self.weights.append(1)

    def get_predictions(self, data):
        return np.array([e.predict(data) for e in self.experts])

    def get_weighted_majority(self, preds):
        return np.sign(np.dot(preds,np.array(self.weights)))

    def update_weights(self, preds, outcome):
        for (i,p) in enumerate(preds):
            if p!=outcome:
                self.weights[i]*=self.beta

    def get_outcome(self, data, t):
        assert 0 < t < len(data)
        if data[t]>data[t-1]:
            return 1
        return -1

    def do_alg(self, data, start_pos = 20):
        n_mistakes = 0
        reward = 0

        for i in range(len(data)):
            t = i+start_pos
            temp = data[ : t]
            preds = self.get_predictions(temp)
            decision = self.get_weighted_majority(preds)
            outcome = self.get_outcome(data, t)
            rew = np.absolute(data[t]-data[t-1])
            if decision == outcome:
                reward += rew
            else:
                reward -= rew
        return reward



