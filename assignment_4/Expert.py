from abc import ABC, abstractmethod
import numpy as np


class Expert(ABC):

    def predict(self, *args, **kwargs):
        pass

class MeanReversion(Expert):

    def predict(self, data, window=20):
        window = max(window,len(data))
        if data[-1]>np.mean(data[-window:]):
            return -1
        return 1

def moving_average(self, a, n=6) :
    ret = np.cumsum(a, dtype=float)
    ret[n :] = ret[n :] - ret[:-n]
    return ret[n - 1 :] / n